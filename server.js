/**
 * Created by Laeelin on 3/26/14.
 */

// Load database
var mongoose= require('./app/database');

// Load App
var express = require('express');
var app = express();
var socketio = require('socket.io');

// App Config
var appConfig = require('./app/appConfig')(app, express);



// Start Server
var port = process.env.PORT || 3000;  // set our port .. 3000 for http, ssl will be 3443
var server = app.listen(port);
var io = socketio.listen(server);

// Modules
var characters = require('./app/modules/characters/charactersModule')(app, io);
var inventory = require('./app/modules/inventory/inventoryModule')(app, io);
var recipes = require('./app/modules/recipes/recipesModule')(app, io);


// Final Routes
var appRoutes = require('./app/appRoutes')(app);


console.log('Express server started on port %s', port);

