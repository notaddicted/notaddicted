angular.module('craftyApp').config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    "use strict";
    
	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		});

		// example
		/*.when('/example', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		});*/

	$locationProvider.html5Mode(true);

}]);