(function(){
    "use strict";
    angular.module('craftyApp').factory('SocketIO', function(socketFactory) {
        return socketFactory();
    });
})();