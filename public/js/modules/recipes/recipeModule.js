angular.module('recipeModule',
    [
        'ngRoute',
        'recipesRoutes',
        'MainRecipeController',
        'ListRecipeController',
        'DetailRecipeController',
        'UpdateRecipeController',
        'DeleteRecipeController',
        'NewRecipeController',
        'RecipeService'
    ]
);