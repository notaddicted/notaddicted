angular.module('RecipeService', []).factory('Recipe', ['$http', function($http) {
    "use strict";

	//noinspection ReservedWordAsName
    return {

		get : function() {
            console.log('getting recipes');
			return $http.get('/api/recipes');
		},

        getOne : function(id) {
            return $http.get('/api/recipes/' + id);
        },

		create : function(recipeData) {
			return $http.post('/api/recipes', recipeData);
		},

        update : function(id, recipeData) {
            return $http.put('/api/recipes/' + id, recipeData);
        },

		delete : function(id) {
			return $http.delete('/api/recipes/' + id);
		}
	};
}]);