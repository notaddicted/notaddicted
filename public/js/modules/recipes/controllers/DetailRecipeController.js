angular.module('DetailRecipeController', []).controller('DetailRecipeController', function($scope, $routeParams, Recipe) {
    "use strict";
    $scope.recipe = {};
    $scope.id = $routeParams.id;

    Recipe.getOne($routeParams.id)
        .success(function(data) {
               $scope.recipe = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });


});