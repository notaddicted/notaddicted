angular.module('DeleteRecipeController', []).controller('DeleteRecipeController', function($scope, $routeParams, $location, Recipe) {
    "use strict";

    $scope.recipe = {};
    $scope.id = $routeParams.id;

    Recipe.getOne($routeParams.id)
        .success(function(data) {
            $scope.recipe = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });

    $scope.deleteItem = function() {
        Recipe.delete($scope.id).success(function() {
            $scope.recipe = {};
            $scope.id = {};
            $location.path( "/recipes" );
        });
    };

});