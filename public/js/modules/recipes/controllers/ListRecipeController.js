angular.module('ListRecipeController', []).controller('ListRecipeController', function($scope, Recipe, SocketIO) {
    "use strict";

    SocketIO.on('recipe:add', function (data) {
        console.log("socket - recipe:add");
        $scope.recipes.push(data.recipe);
    });


    SocketIO.on('recipe:delete', function (data) {

        for (var i = 0; i < $scope.recipes.length; i++) {
            var recipe = $scope.recipes[i];
            if (recipe._id === data._id) {
                $scope.recipes.splice(i, 1);
                break;
            }
        }
    });

    SocketIO.on('recipe:update', function (data) {

        for (var i = 0; i < $scope.recipes.length; i++) {
            var recipe = $scope.recipes[i];
            if (recipe._id === data.recipe._id) {
                $scope.recipes[i] = data.recipe;
                break;
            }
        }
    });

    Recipe.get()
        .success(function(data) {
            $scope.recipes = data;
            console.log(data);
        });
});