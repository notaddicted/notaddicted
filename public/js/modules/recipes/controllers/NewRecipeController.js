angular.module('NewRecipeController', []).controller('NewRecipeController', function($scope, $location, Recipe) {
    "use strict";

    $scope.newRecipe = {};

	$scope.save = function(newRecipe) {
        Recipe.create(newRecipe)
            .success(function(data) {
                $scope.recipes = data;
                $scope.recipe = {};
                $location.path( "/recipes" );
            });
    };
});