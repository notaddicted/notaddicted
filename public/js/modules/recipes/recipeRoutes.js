angular.module('recipesRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    "use strict";

    $routeProvider
        .when('/recipes', {
            templateUrl: '/js/modules/recipes/views/main.html',
            controller: 'MainRecipeController'
        })

        .when('/recipes/list', {
            templateUrl: '/js/modules/recipes/views/list.html',
            controller: 'ListRecipeController'
        })

        .when('/recipes/detail/:id', {
            templateUrl: '/js/modules/recipes/views/detail.html',
            controller: 'DetailRecipeController'
        })

        .when('/recipes/update/:id', {
            templateUrl: '/js/modules/recipes/views/update.html',
            controller: 'UpdateRecipeController'
        })

        .when('/recipes/new', {
            templateUrl: '/js/modules/recipes/views/new.html',
            controller: 'NewRecipeController'
        })

        .when('/recipes/delete/:id', {
            templateUrl: '/js/modules/recipes/views/delete.html',
            controller: 'DeleteRecipeController'
        });

    $locationProvider.html5Mode(true);

}]);