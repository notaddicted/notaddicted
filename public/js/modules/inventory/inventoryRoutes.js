angular.module('inventoryRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    "use strict";

    $routeProvider
        .when('/inventory', {
            templateUrl: '/js/modules/inventory/views/main.html',
            controller: 'MainInventoryController'
        })

        .when('/inventory/list', {
            templateUrl: '/js/modules/inventory/views/list.html',
            controller: 'ListInventoryController'
        })

        .when('/inventory/detail/:id', {
            templateUrl: '/js/modules/inventory/views/detail.html',
            controller: 'DetailItemController'
        })

        .when('/inventory/update/:id', {
            templateUrl: '/js/modules/inventory/views/update.html',
            controller: 'UpdateItemController'
        })

        .when('/inventory/new', {
            templateUrl: '/js/modules/inventory/views/new.html',
            controller: 'NewItemController'
        })

        .when('/inventory/delete/:id', {
            templateUrl: '/js/modules/inventory/views/delete.html',
            controller: 'DeleteItemController'
        });

    $locationProvider.html5Mode(true);

}]);