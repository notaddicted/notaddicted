angular.module('UpdateItemController', []).controller('UpdateItemController', function($scope, $routeParams, $location, Inventory) {
    "use strict";

    $scope.item = {};
    $scope.id = $routeParams.id;

    Inventory.getOne($routeParams.id)
        .success(function(data) {
           $scope.item = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });

    $scope.update = function(item) {
        Inventory.update($scope.id, item).success(function() {
            $scope.item = {};
            $scope.id = {};
            $location.path( "/inventory" );
        });
    };
});