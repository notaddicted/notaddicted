angular.module('NewItemController', []).controller('NewItemController', function($scope, $location, Inventory) {
    "use strict";

    $scope.newItem = {};

	$scope.save = function(newItem) {
        Inventory.create(newItem)
            .success(function(data) {
                $scope.item = data;
                $scope.item = {};
                $location.path( "/inventory" );
            });
    };
});