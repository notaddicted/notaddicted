angular.module('ListInventoryController', []).controller('ListInventoryController', function($scope, Inventory, SocketIO) {
    "use strict";

    SocketIO.on('inventory:add', function (data) {
        console.log("socket - inventory:add");
        $scope.inventory.push(data.item);
    });


    SocketIO.on('inventory:delete', function (data) {

        for (var i = 0; i < $scope.inventory.length; i++) {
            var item = $scope.inventory[i];
            if (item._id === data._id) {
                $scope.inventory.splice(i, 1);
                break;
            }
        }
    });

    SocketIO.on('inventory:update', function (data) {

        for (var i = 0; i < $scope.inventory.length; i++) {
            var item = $scope.inventory[i];
            if (item._id === data.item._id) {
                $scope.inventory[i] = data.item;
                break;
            }
        }
    });

    Inventory.get()
        .success(function(data) {
            $scope.inventory = data;
            console.log(data);
        });
});