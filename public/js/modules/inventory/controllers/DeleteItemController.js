angular.module('DeleteItemController', []).controller('DeleteItemController', function($scope, $routeParams, $location, Inventory) {
    "use strict";

    $scope.item = {};
    $scope.id = $routeParams.id;

    Inventory.getOne($routeParams.id)
        .success(function(data) {
            $scope.item = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });

    $scope.deleteItem = function() {
        Inventory.delete($scope.id).success(function() {
            $scope.item = {};
            $scope.id = {};
            $location.path( "/inventory" );
        });
    };

});