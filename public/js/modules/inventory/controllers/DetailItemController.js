angular.module('DetailItemController', []).controller('DetailItemController', function($scope, $routeParams, Inventory) {
    "use strict";
    $scope.item = {};
    $scope.id = $routeParams.id;

    Inventory.getOne($routeParams.id)
        .success(function(data) {
               $scope.item = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });


});