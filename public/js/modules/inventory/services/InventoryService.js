angular.module('InventoryService', []).factory('Inventory', ['$http', function($http) {
    "use strict";

	//noinspection ReservedWordAsName
    return {

		get : function() {
            console.log('getting inventory');
			return $http.get('/api/inventory');
		},

        getOne : function(id) {
            return $http.get('/api/inventory/' + id);
        },

		create : function(itemData) {
			return $http.post('/api/inventory', itemData);
		},

        update : function(id, itemData) {
            return $http.put('/api/inventory/' + id, itemData);
        },

		delete : function(id) {
			return $http.delete('/api/inventory/' + id);
		}
	};
}]);