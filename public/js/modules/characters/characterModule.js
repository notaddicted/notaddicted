angular.module('characterModule',
    [
        'ngRoute',
        'characterRoutes',
        'MainCharacterController',
        'ListCharacterController',
        'DetailCharacterController',
        'UpdateCharacterController',
        'DeleteCharacterController',
        'NewCharacterController',
        'CharacterService',
        'AttributesService'
    ]
);