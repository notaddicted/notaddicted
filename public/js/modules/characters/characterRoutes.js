angular.module('characterRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    "use strict";
    $routeProvider
        .when('/characters', {
            templateUrl: '/js/modules/characters/views/main.html',
            controller: 'MainCharacterController'
        })

        .when('/characters/list', {
            templateUrl: '/js/modules/characters/views/list.html',
            controller: 'ListCharacterController'
        })

        .when('/characters/detail/:id', {
            templateUrl: '/js/modules/characters/views/detail.html',
            controller: 'DetailCharacterController'
        })

        .when('/characters/update/:id', {
            templateUrl: '/js/modules/characters/views/update.html',
            controller: 'UpdateCharacterController'
        })

        .when('/characters/new', {
            templateUrl: '/js/modules/characters/views/new.html',
            controller: 'NewCharacterController'
        })

        .when('/characters/delete/:id', {
            templateUrl: '/js/modules/characters/views/delete.html',
            controller: 'DeleteCharacterController'
        });

    $locationProvider.html5Mode(true);

}]);