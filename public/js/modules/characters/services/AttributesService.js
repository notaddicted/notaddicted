angular.module('AttributesService', []).factory('Attributes', ['$http', function($http) {
    "use strict";

    return {
        getStats: function getStats() {
            return [
                {
                    label: "Physical Attributes",
                    stats: {
                        strength: { label: "Strength", value: 100},
                        agility: { label: "Agility", value: 100},
                        toughness: { label: "Toughness", value: 100}
                    }
                },
                {
                    label: "Mental Attributes",
                    stats: {
                        intelligence: {label: "Intelligence", value: 100},
                        magic: { label: "Magic", value: 100},
                        mana: { label: "Mana", value: 100}
                    }
                },
                {
                    label: "Social Attributes",
                    stats: {
                        presence: { label: "Presence", value: 100},
                        charm: { label: "Charm", value: 100},
                        intimidation: { label: "Intimidation", value: 100}
                    }
                },
                {
                    label: "Special Attributes",
                    stats: {
                        health: { label: "Health", value: 100,  priority: 10},
                        luck: { label: "Luck", value: 100,  priority: 20},
                        speed: { label: "Speed", value: 100,  priority: 30}
                    }
                }
            ];
        },
        getSkills: function getSkills() {
            return [
                {
                    label: "Crafting",
                    skills: {
                        metalworking: { label: "Metalworking", value: 100},
                        woodworking: { label: "Woodworking", value: 100},
                        herbalist: { label: "Herbalist", value: 100}
                    }
                },
                {
                    label: "Gathering",
                    skills: {
                        mining: { label: "Mining", value: 100},
                        farming: { label: "Farming", value: 100},
                        woodcutting: { label: "Woodcutting", value: 100}
                    }
                }
            ];
        },
        getQuestingSkills: function getQuestingSkills() {
            return [
                {
                    label: "Fighting",
                    skills: {
                        attack: { label: "Attack", value: 100},
                        damage: { label: "Damage", value: 100},
                        defense: { label: "Defense", value: 100}
                    }
                },
                {
                    label: "Exploring",
                    skills: {
                        search: { label: "Search", value: 100},
                        traps: { label: "Find Traps", value: 100},
                        escape: { label: "Escape", value: 100}
                    }
                }
            ];
        }
	};
}]);