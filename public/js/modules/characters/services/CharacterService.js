angular.module('CharacterService', []).factory('Character', ['$http', function($http) {
    "use strict";

	//noinspection ReservedWordAsName
    return {

		get : function() {
            console.log('getting characters');
			return $http.get('/api/characters');
		},

        getOne : function(id) {
            return $http.get('/api/characters/' + id);
        },

		create : function(characterData) {
			return $http.post('/api/characters', characterData);
		},

        update : function(id, characterData) {
            return $http.put('/api/characters/' + id, characterData);
        },

		delete : function(id) {
			return $http.delete('/api/characters/' + id);
		}
	};
}]);