angular.module('ListCharacterController', []).controller('ListCharacterController', function($scope, Character, SocketIO) {
    "use strict";

    SocketIO.on('character:add', function (data) {
        console.log("socket - character:add");
        $scope.characters.push(data.character);
    });


    SocketIO.on('character:delete', function (data) {

        for (var i = 0; i < $scope.characters.length; i++) {
            var character = $scope.characters[i];
            if (character._id === data._id) {
                $scope.characters.splice(i, 1);
                break;
            }
        }
    });

    SocketIO.on('character:update', function (data) {

        for (var i = 0; i < $scope.characters.length; i++) {
            var character = $scope.characters[i];
            if (character._id === data.character._id) {
                $scope.characters[i] = data.character;
                break;
            }
        }
    });

    Character.get()
        .success(function(data) {
            $scope.characters = data;
            console.log(data);
        });
});