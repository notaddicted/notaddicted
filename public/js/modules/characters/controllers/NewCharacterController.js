angular.module('NewCharacterController', []).controller('NewCharacterController', function($scope, $location, Character, Attributes) {
    "use strict";

    $scope.newCharacter = {
        statTypes: Attributes.getStats(),
        skillTypes: Attributes.getSkills(),
        questingSkillTypes: Attributes.getQuestingSkills()
    };

	$scope.save = function(newCharacter) {
        Character.create(newCharacter)
            .success(function(data) {
                $scope.characters = data;
                $scope.character = {};
                $location.path( "/characters" );
            });
    };

});



