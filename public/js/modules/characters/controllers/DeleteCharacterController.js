angular.module('DeleteCharacterController', []).controller('DeleteCharacterController', function($scope, $routeParams, $location, Character) {
    "use strict";
    $scope.character = {};
    $scope.id = $routeParams.id;

    Character.getOne($routeParams.id)
        .success(function(data) {
            $scope.character = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });

    $scope.deleteItem = function() {
        Character.delete($scope.id).success(function() {
            $scope.character = {};
            $scope.id = {};
            $location.path( "/characters" );
        });
    };

});