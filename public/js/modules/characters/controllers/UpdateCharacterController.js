angular.module('UpdateCharacterController', []).controller('UpdateCharacterController', function($scope, $routeParams, $location, Character) {
    "use strict";
    $scope.character = {};
    $scope.id = $routeParams.id;

    Character.getOne($routeParams.id)
        .success(function(data) {
           $scope.character = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });

    $scope.update = function(character) {
        Character.update(character.id, character).success(function() {
            $scope.character = {};
            $scope.id = {};
            $location.path( "/characters" );
        });
    };
});