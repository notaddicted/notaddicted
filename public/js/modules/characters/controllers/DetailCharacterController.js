angular.module('DetailCharacterController', []).controller('DetailCharacterController', function($scope, $routeParams, Character) {
    "use strict";
    $scope.character = {};
    $scope.id = $routeParams.id;

    Character.getOne($routeParams.id)
        .success(function(data) {
               $scope.character = data;
        })
        .error(function (data,status){
            console.log(data + status);
        });


});