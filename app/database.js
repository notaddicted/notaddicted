/**
 * Created by Laeelin on 3/27/14.
 */


var db = require('../config/db');
var mongoose= require('mongoose');

mongoose.connect(db.url, function (err, res) {
    "use strict";
    if (err) {
        console.log ('ERROR connecting to: ' + db.url + '. ' + err);
    } else {
        console.log ('Succeeded connected to database: ' + db.url);
    }
});


module.exports = mongoose;