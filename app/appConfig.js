

module.exports = function(app, express) {
    "use strict";
    app.use(express.static(__dirname + '/../public'));     // set the static files location /public/img will be /img for users
    app.use(express.bodyParser());                      // have the ability to pull information from html in POST
    app.use(express.methodOverride());                  // have the ability to simulate DELETE and PUT
    app.use(express.logger('dev'));                     // log every request to the console
};
