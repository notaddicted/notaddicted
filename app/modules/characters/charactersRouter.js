/**
 * Created by Laeelin on 3/27/14.
 */

//Create Routes
module.exports = function(app, io, models) {
    "use strict";
    var Character = models.Character;

    app.get('/api/characters', function(req, res) {
        Character.find(function(err, characters) {
            if (!err) {
                console.log('Listing Characters:' + characters.length);
                res.json(200, characters);
            } else {
                console.log('ERR Characters not found!');
                res.send(404, err);
            }
        });
    });

    app.get('/api/characters/:id', function(req, res) {
        Character.findOne({"_id" : req.params.id}, function(err, character) {
            if (!err) {
                console.log('Character Detail:' + JSON.stringify(character));
                res.json(200, character);
            } else {
                console.log('ERR Character not found: ' + req.params.id);
                res.send(404, err);
            }
        });
    });

    app.post('/api/characters', function(req, res) {

        var newCharacter = new Character (req.body);
        newCharacter.save(function (err) {
            if (!err) {
                console.log('Created Character:' + JSON.stringify(req.body));
                io.sockets.emit('character:add', { "character": newCharacter });
                res.send(200, {}, { action: 'create' });
            } else {
                console.log('ERR Error creating character!' + JSON.stringify(req.body));
                res.send(404, err);
            }
        });
    });

    app.put('/api/characters/:id', function(req, res) {
        Character.findOne({"_id" : req.params.id}, function(err, character) {
            character.updateCharacter(function (err) {
                if (!err) {
                    console.log('Updated Character:' + JSON.stringify(req.body));
                    io.sockets.emit('character:update', { "character":  character });
                    res.send(200, {}, { action: 'create' });
                } else {
                    console.log('ERR Error updating character!' + err);
                    res.send(404, err);
                }
            }, req.body);
        });
    });

    app.delete('/api/characters/:id', function(req, res) {
        console.log('Delete Character:' + JSON.stringify(req.body));

        Character.findOne({"_id" : req.params.id}, function(err, character) {
            if (!err) {
                character.remove();
                console.log('Character deleted:' + req.params.id);
                io.sockets.emit('character:delete', { "_id": req.params.id });
                res.json(200, character);
            } else {
                console.log('ERR Character not deleted: ' + req.params.id);
                res.send(404, err);
            }
        });

    });

    console.log('Module Characters: Routes loaded');

};
