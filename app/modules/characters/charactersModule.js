/**
 * Created by Laeelin on 3/27/14.
 */

module.exports = function(app, io) {
    "use strict";
    console.log('Module Character: Loading');

    var models = {};
    models.Character =  require('./models/Character');

    var routes = require('./charactersRouter')(app, io, models);
};