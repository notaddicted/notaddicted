/*
    Characters:

    Stats:
        !! NOTE: The stats listed here are the base stats before any modification.

        Stats are percentages compared to standard (100% is average). For example 100% strength is average strength.
        150% would be 50% stronger than normal. Equipment, quests, buffs, curses, etc all can effect these base stats.
        Things that effect stats are added and subtracted. For example, boots of speed might give "50%" speed. This
        would give a person with 200 speed 250 and a person with 50 speed 100. A -20% speed curse would give 180% and 230%
        speed to the characters in the example above.

        Order of application doesn't make a difference, and this causes a natural diminishing returns.

        There is also a min (.25) / max ( 2 ) ( for stats.

        Stats:
            health

            magic
            mana
            intelligence


            speed
            strength
            agility

            presence
            charm
            intimidation

            Luck: A chance that you get to reroll and keep the higher roll - 100% is one roll..
                110% is 10 chance for an extra roll and keep the best roll.
                90% is 10 chance for an extra roll and keep the worse roll!

        This is heavily modified by profession choices and gear.

        Crafting Skills:
            metalworking
            woodworking
            herbalist

        Gathering Skills:
            mining
            farming    
            woodcutting
            animal husbandry

        Fighting Skills:
            attack
            damage
            defense
 */



var mongoose= require('../../../database');


var CharacterSchema = new mongoose.Schema({
    firstName: { type: String, trim: true },
    lastName: { type: String, trim: true },
    experience: { type: Number, min: 0},
    profession: { type: String, trim: true },
    species: { type: String, trim: true },
    sex: { type: String, trim: true, enum: ['Male', 'Female', 'Neuter'] },
    stats: {
        health: { type: Number, min: 0.25, max: 5},
        magic: { type: Number, min: 0.25, max: 5},
        mana: { type: Number, min: 0.25, max: 5},
        intelligence: { type: Number, min: 0.25, max: 5},
        speed: { type: Number, min: 0.25, max: 5},
        toughness: { type: Number, min: 0.25, max: 5},
        strength: { type: Number, min: 0.25, max: 5},
        agility: { type: Number, min: 0.25, max: 5},
        presence:{ type: Number, min: 0.25, max: 5},
        charm: { type: Number, min: 0.25, max: 5},
        intimidation: { type: Number, min: 0.25, max: 5},
        luck: { type: Number, min: 0.25, max: 5}
    },
    craftingSkills: {
        metalworking: { type: Number, min: 0.25, max: 5},
        woodworking: { type: Number, min: 0.25, max: 5},
        herbalist: { type: Number, min: 0.25, max: 5}
    },
    gatheringSkills: {
        mining: { type: Number, min: 0.25, max: 5},
        farming: { type: Number, min: 0.25, max: 5},
        woodcutting: { type: Number, min: 0.25, max: 5}
    },
    fightingSkills: {
        attack: { type: Number, min: 0.25, max: 5},
        damage: { type: Number, min: 0.25, max: 5},
        defense: { type: Number, min: 0.25, max: 5}
    },
    exploringSkills: {
        search: { type: Number, min: 0.25, max: 5},
        traps: { type: Number, min: 0.25, max: 5},
        escape: { type: Number, min: 0.25, max: 5}
    },

    created: {type: Date, default: Date.now},
    updated: {type: Date, default: Date.now}
});

CharacterSchema.methods.updateCharacter = function (err, character) {
    "use strict";
    this.firstName = character.firstName;
    this.lastName = character.lastName;
    this.updated = Date.now;
    this.save(err);
};

CharacterSchema.methods.createCharacter = function (err, character) {
    "use strict";
    this.firstName = character.firstName;
    this.lastName = character.lastName;
    this.professionId = character.professionId;
    this.sex = character.sex;
    this.species = character.species;

    if (this.checkStats(character.stats)) {
        this.stats = character.stats;
    }

    if (this.checkSkills(character.skills)) {
        this.skills = character.skills;
    }

    if (this.checkFightingSkills(character.fightingSkills)) {
        this.fightingSkills = character.fightingSkills;
    }

    this.save(err);
};

CharacterSchema.methods.checkStats = function (stats) {
    "use strict";
    return true;
};

CharacterSchema.methods.checkSkills = function (skills) {
    "use strict";
    return true;
};

CharacterSchema.methods.checkFightingSkills = function (skills) {
    "use strict";
    return true;
};

module.exports = mongoose.model('Characters', CharacterSchema);

console.log('Module Characters: Model Character loaded');
