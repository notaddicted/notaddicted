/*

 Environment:

    This is the general "look and feel" of the world the character in is.

    Some examples include:
        SciFi
        Fantasy
        Western
        Modern

 */


var mongoose= require('../../../database');


var EnvironmentSchema = new mongoose.Schema({
    name: { type: String, trim: true },
    skin: { type: String, trim: true }
});

EnvironmentSchema.methods.updateCharacter = function (err, environment) {
    "use strict";
    this.name = environment.name;
    this.name = environment.skin;
    this.save(err);
};

module.exports = mongoose.model('environment', EnvironmentSchema);


console.log('Module Characters: Model Environment loaded');
