/*

 Species:

 Species give bonuses / penalties to stats.

    Some examples include:
        Human: No Stat Change,
        Unicorn: +magic, +strength, -trade
 */


var mongoose= require('../../../database');


var SpeciesSchema = new mongoose.Schema({
    name: { type: String, trim: true },
    stats: {
        health: { type: Number, min: 0.25, max: 5},
        attack: { type: Number, min: 0.25, max: 5},
        damage: { type: Number, min: 0.25, max: 5},
        defense: { type: Number, min: 0.25, max: 5},
        magic: { type: Number, min: 0.25, max: 5},
        mana: { type: Number, min: 0.25, max: 5},
        speed: { type: Number, min: 0.25, max: 5},
        strength: { type: Number, min: 0.25, max: 5},
        agility: { type: Number, min: 0.25, max: 5},
        presence:{ type: Number, min: 0.25, max: 5},
        charm: { type: Number, min: 0.25, max: 5},
        intimidation: { type: Number, min: 0.25, max: 5},
        luck: { type: Number, min: 0.25, max: 5}
    },
    skills: {
        metalworking: { type: Number, min: 0.25, max: 5},
        woodworking: { type: Number, min: 0.25, max: 5},
        herbalist: { type: Number, min: 0.25, max: 5},
        mining: { type: Number, min: 0.25, max: 5},
        farming: { type: Number, min: 0.25, max: 5},
        woodcutting: { type: Number, min: 0.25, max: 5},
        animalHusbandry: { type: Number, min: 0.25, max: 5}
    }
});

SpeciesSchema.methods.updateSpecies = function (err, species) {
    "use strict";
    this.name = species.name;
    this.save(err);
};

module.exports = mongoose.model('Species', SpeciesSchema);


console.log('Module Characters: Model Species loaded');
