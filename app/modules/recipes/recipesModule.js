/**
 * Created by Laeelin on 3/27/14.
 */

module.exports = function(app, io) {
    "use strict";
    console.log('Module Recipes: Loading');

    var models = {};
    models.Recipe =  require('./models/Recipe');

    var routes = require('./recipesRouter')(app, io, models);
};