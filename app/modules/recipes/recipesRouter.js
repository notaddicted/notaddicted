/**
 * Created by Laeelin on 3/27/14.
 */

//Create Routes
module.exports = function(app, io, models) {
    "use strict";
    var Recipe = models.Recipe;

    app.get('/api/recipes', function(req, res) {
        Recipe.find(function(err, recipes) {
            if (!err) {
                console.log('Listing Recipes:' + recipes.length);
                res.json(200, recipes);
            } else {
                console.log('ERR Recipes not found!');
                res.send(404, err);
            }
        });
    });

    app.get('/api/recipes/:id', function(req, res) {
        Recipe.findOne({"_id" : req.params.id}, function(err, recipe) {
            if (!err) {
                console.log('Recipe Detail:' + JSON.stringify(recipe));
                res.json(200, recipe);
            } else {
                console.log('ERR Recipe not found: ' + req.params.id);
                res.send(404, err);
            }
        });
    });

    app.post('/api/recipes', function(req, res) {

        var newRecipe = new Recipe (req.body);
        newRecipe.save(function (err) {
            if (!err) {
                console.log('Created Recipe:' + JSON.stringify(req.body));
                io.sockets.emit('recipe:add', { "recipe": newRecipe });
                res.send(200, {}, { action: 'create' });
            } else {
                console.log('ERR Error creating recipe!' + JSON.stringify(req.body));
                res.send(404, err);
            }
        });
    });

    app.put('/api/recipes/:id', function(req, res) {
        Recipe.findOne({"_id" : req.params.id}, function(err, recipe) {
            recipe.updateRecipe(function (err) {
                if (!err) {
                    console.log('Updated Recipe:' + JSON.stringify(req.body));
                    io.sockets.emit('recipe:update', { "recipe":  recipe });
                    res.send(200, {}, { action: 'create' });
                } else {
                    console.log('ERR Error updating recipe!' + err);
                    res.send(404, err);
                }
            }, req.body);
        });
    });

    app.delete('/api/recipes/:id', function(req, res) {
        console.log('Delete Recipe:' + JSON.stringify(req.body));

        Recipe.findOne({"_id" : req.params.id}, function(err, recipe) {
            if (!err) {
                recipe.remove();
                console.log('Recipe deleted:' + req.params.id);
                io.sockets.emit('recipe:delete', { "_id": req.params.id });
                res.json(200, recipe);
            } else {
                console.log('ERR Recipe not deleted: ' + req.params.id);
                res.send(404, err);
            }
        });

    });

    console.log('Module Recipes: Routes loaded');

};
