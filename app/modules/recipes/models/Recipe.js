var mongoose= require('../../../database');


var RecipeSchema = new mongoose.Schema({
    name: { type: String, trim: true }
});

RecipeSchema.methods.updateRecipe = function (err, recipe) {
    "use strict";
    this.name = recipe.name;
    this.save(err);
};

module.exports = mongoose.model('Recipes', RecipeSchema);

console.log('Module Recipes: Model Recipe loaded');
