/**
 * Created by Laeelin on 3/27/14.
 */

//Create Routes
module.exports = function(app, io, models) {
    "use strict";
    var Inventory = models.Inventory;

    app.get('/api/inventory', function(req, res) {
        Inventory.find(function(err, inventory) {
            if (!err) {
                console.log('Listing Inventory:' + inventory.length);
                res.json(200, inventory);
            } else {
                console.log('ERR Inventory not found!');
                res.send(404, err);
            }
        });
    });

    app.get('/api/inventory/:id', function(req, res) {
        Inventory.findOne({"_id" : req.params.id}, function(err, inventory) {
            if (!err) {
                console.log('Inventory Detail:' + JSON.stringify(inventory));
                res.json(200, inventory);
            } else {
                console.log('ERR Inventory not found: ' + req.params.id);
                res.send(404, err);
            }
        });
    });

    app.post('/api/inventory', function(req, res) {

        var newItem = new Inventory (req.body);
        newItem.save(function (err) {
            if (!err) {
                console.log('Created Inventory:' + JSON.stringify(req.body));
                io.sockets.emit('inventory:add', { "item": newItem });
                res.send(200, {}, { action: 'create' });
            } else {
                console.log('ERR Error creating item!' + JSON.stringify(req.body));
                res.send(404, err);
            }
        });
    });

    app.put('/api/inventory/:id', function(req, res) {
        Inventory.findOne({"_id" : req.params.id}, function(err, item) {
            item.updateItem(function (err) {
                if (!err) {
                    console.log('Updated Item:' + JSON.stringify(req.body));
                    io.sockets.emit('inventory:update', { "item":  item });
                    res.send(200, {}, { action: 'create' });
                } else {
                    console.log('ERR Error updating item!' + err);
                    res.send(404, err);
                }
            }, req.body);
        });
    });

    app.delete('/api/inventory/:id', function(req, res) {
        console.log('Delete Item:' + JSON.stringify(req.body));

        Inventory.findOne({"_id" : req.params.id}, function(err, item) {
            if (!err) {
                item.remove();
                console.log('Item deleted:' + req.params.id);
                io.sockets.emit('inventory:delete', { "_id": req.params.id });
                res.json(200, item);
            } else {
                console.log('ERR Item not deleted: ' + req.params.id);
                res.send(404, err);
            }
        });

    });

    console.log('Module Inventory: Routes loaded');

};
