var mongoose= require('../../../database');


var ItemTypeSchema = new mongoose.Schema({
    name: { type: String, trim: true },
    type: { type: String, trim: true },
    subType: { type: String, trim: true }
});

ItemTypeSchema.methods.updateItemType = function (err, itemType) {
    "use strict";
    this.name = itemType.name;
    this.type = itemType.type;
    this.subType = itemType.type;
    this.save(err);
};

module.exports = mongoose.model('ItemType', ItemTypeSchema);

console.log('Module Inventory: Model ItemType loaded');
