var mongoose= require('../../../database');


var ItemSchema = new mongoose.Schema({
    name: { type: String, trim: true },
    qty: { type: Number, trim: true },
    quality: { type: Number, trim: true }
});

ItemSchema.methods.updateItem = function (err, item) {
    "use strict";
    this.name = item.name;
    this.qty = item.qty;
    this.save(err);
};

module.exports = mongoose.model('Inventory', ItemSchema);

console.log('Module Inventory: Model Item loaded');
