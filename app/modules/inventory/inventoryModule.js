/**
 * Created by Laeelin on 3/27/14.
 */

module.exports = function(app, io) {
    "use strict";
    console.log('Module Inventory: Loading');

    var models = {};
    models.Inventory =  require('./models/Inventory');

    var routes = require('./inventoryRouter')(app, io, models);
};